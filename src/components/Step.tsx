import * as React from 'react';
import styled from 'styled-components';

interface IStep {
  next?: () => void;
  back?: () => void;
  children: React.ReactNode;
}

const Button = styled.button``;

const Wrapper = styled.div`
  flex: 1;
`;

const Text = styled.text``;

function Step({ children, next, back }: IStep) {
  return (
    <Wrapper>
      {children}
      <Button onClick={back}>
        <Text>Volver</Text>
      </Button>
      <Button onClick={next}>
        <Text>siguiente</Text>
      </Button>
    </Wrapper>
  );
}

export default Step;
