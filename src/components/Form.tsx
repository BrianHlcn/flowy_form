import * as React from 'react';
import Stepper from 'react-stepper-horizontal';
import styled from 'styled-components';

const Text = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: black;
`;

const Wrapper = styled.div`
  flex: 1;
`;

function Form() {
  const [currentStep, setCurrentStep] = React.useState<number>(0);
  const [steps, setSteps] = React.useState<any[]>();
  const data = [
    {
      id: '5de6e7cf26811c001b8f2812',
      title: 'DENUNCIA CIUDADANA',
      enabled: true,
      status: [
        {
          f_id: 's0',
          title: 'Sin Guardar',
          color: 'gray',
          actions: [
            {
              f_id: 's1',
              title: 'Guardado',
              color: 'red',
              action: 's1',
              status: 's1'
            }
          ],
          id: 0
        },
        {
          id: 1,
          f_id: 's1',
          title: 'Guardado',
          actions: [
            {
              f_id: 's2',
              title: 'En seguimiento',
              color: 'green',
              action: 's2',
              status: 's2'
            }
          ],
          indexColor: 0,
          color: 'white'
        },
        {
          id: 2,
          f_id: 's2',
          title: 'En seguimiento',
          actions: [
            {
              f_id: 's3',
              title: 'Caso cerrado',
              color: 'red',
              action: 's3',
              status: 's3'
            }
          ],
          color: 'green',
          indexColor: 3
        },
        {
          id: 3,
          f_id: 's3',
          title: 'Caso cerrado',
          actions: [
            {
              f_id: 's1',
              title: 'Guardado',
              color: 'white',
              action: 's1',
              status: 's1'
            }
          ],
          color: 'red',
          indexColor: 4
        }
      ],
      configuration: null,
      variables: [],
      catalogs: [
        {
          f_id: '5de6e7cf26811c001b8f2812-1575417143274',
          f_index: 2,
          online: false,
          TOTAL_OPTIONS: '1',
          OPTIONS_PER_PAGE: '5',
          options: [
            {
              _id: '5de6f5311287dc001b214d82',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575417143274',
              f_id: '5de6f5311287dc001b214d82',
              value: 'CIUDAD DE MÉXICO',
              enable: true
            }
          ],
          currentPage: '1'
        },
        {
          f_id: '5de6e7cf26811c001b8f2812-1575486262108',
          f_index: 2,
          online: false,
          TOTAL_OPTIONS: '2',
          OPTIONS_PER_PAGE: '5',
          options: [
            {
              _id: '5de8031bb0366b001b48b5fe',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575486262108',
              f_id: '5de8031bb0366b001b48b5fe',
              value: 'MASCULINO',
              enable: true
            },
            {
              _id: '5de8031bb0366b001b48b5ff',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575486262108',
              f_id: '5de8031bb0366b001b48b5ff',
              value: 'FEMENINO',
              enable: true
            }
          ],
          currentPage: '1'
        },
        {
          options: {
            '5de6f5311287dc001b214d82': [
              {
                _id: '5de6f54877abd7001bc04233',
                formId: '5de6e7cf26811c001b8f2812',
                catalogFid: '5de6e7cf26811c001b8f2812-1575417194395',
                f_id: '5de6f54877abd7001bc04233',
                value: 'ALVARO OBREGON',
                enable: true,
                edit: true,
                parentOptionId: '5de6f5311287dc001b214d82'
              },
              {
                _id: '5de7ffbbb0366b001b48b5fd',
                formId: '5de6e7cf26811c001b8f2812',
                catalogFid: '5de6e7cf26811c001b8f2812-1575417194395',
                value: 'MIGUEL HIDALGO',
                f_id: '5de7ffbbb0366b001b48b5fd',
                edit: true,
                enable: true,
                parentOptionId: '5de6f5311287dc001b214d82'
              }
            ]
          },
          f_id: '5de6e7cf26811c001b8f2812-1575417194395',
          online: false,
          TOTAL_OPTIONS: '2',
          OPTIONS_PER_PAGE: '5',
          currentPage: '1',
          f_index: { '5de6f5311287dc001b214d82': 2 }
        },
        {
          f_id: '5de6e7cf26811c001b8f2812-1575565265304',
          f_index: 14,
          online: false,
          TOTAL_OPTIONS: '12',
          OPTIONS_PER_PAGE: '5',
          options: [
            {
              _id: '5de937e6c01ebc001b4d4dd6',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575565265304',
              f_id: '5de937e6c01ebc001b4d4dd6',
              value: 'ACOSO LABORAL',
              enable: true
            },
            {
              _id: '5de937e6c01ebc001b4d4dd7',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575565265304',
              f_id: '5de937e6c01ebc001b4d4dd7',
              value: 'ACOSO SEXUAL',
              enable: true
            },
            {
              _id: '5de937e6c01ebc001b4d4dd8',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575565265304',
              f_id: '5de937e6c01ebc001b4d4dd8',
              value: 'AGRESIÓN FÍSICA',
              enable: true
            },
            {
              _id: '5de937e6c01ebc001b4d4dd9',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575565265304',
              f_id: '5de937e6c01ebc001b4d4dd9',
              value: 'AGRESIÓN VERBAL',
              enable: true
            },
            {
              _id: '5de937e6c01ebc001b4d4dda',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575565265304',
              f_id: '5de937e6c01ebc001b4d4dda',
              value: 'AMENAZAS',
              enable: true
            },
            {
              _id: '5de937e6c01ebc001b4d4ddb',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575565265304',
              f_id: '5de937e6c01ebc001b4d4ddb',
              value: 'DISCRIMINACIÓN',
              enable: true
            },
            {
              _id: '5de937e6c01ebc001b4d4ddc',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575565265304',
              f_id: '5de937e6c01ebc001b4d4ddc',
              value: 'ESTAFAS',
              enable: true
            },
            {
              _id: '5de937e6c01ebc001b4d4ddd',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575565265304',
              f_id: '5de937e6c01ebc001b4d4ddd',
              value: 'ROBO',
              enable: true
            },
            {
              _id: '5de937e6c01ebc001b4d4dde',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575565265304',
              f_id: '5de937e6c01ebc001b4d4dde',
              value: 'SOBORNO',
              enable: true
            },
            {
              _id: '5de937e6c01ebc001b4d4ddf',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575565265304',
              f_id: '5de937e6c01ebc001b4d4ddf',
              value: 'USO DE INFORMACIÓN RESERVADA',
              enable: true
            },
            {
              _id: '5de937e6c01ebc001b4d4de0',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575565265304',
              f_id: '5de937e6c01ebc001b4d4de0',
              value: 'VIOLENCIA DE GENERO',
              enable: true
            },
            {
              _id: '5de937e6c01ebc001b4d4de1',
              formId: '5de6e7cf26811c001b8f2812',
              catalogFid: '5de6e7cf26811c001b8f2812-1575565265304',
              f_id: '5de937e6c01ebc001b4d4de1',
              value: 'VIOLENCIA DOMESTICA',
              enable: true
            }
          ],
          currentPage: '1'
        }
      ],
      questions: [
        {
          type: 'date',
          title: 'FECHA Y HORA DEL SUCESO',
          edit: false,
          f_id: 'q2',
          enable: true,
          mode: 'datetime',
          number: '1',
          lock: false,
          controldate: true,
          required: true,
          column: true,
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ]
        },
        {
          type: 'multipleOption',
          title: 'TIPO DE HECHO O AGRESIÓN',
          edit: false,
          f_id: 'q36',
          enable: true,
          catalog: '5de6e7cf26811c001b8f2812-1575565265304',
          other: true,
          number: '2',
          other_message: 'OTRO TIPO DE HECHO O AGRESIÓN',
          required: true,
          uppercaseAnswer: true,
          enableHelpMessage: true,
          helpMessage:
            'SEÑALE OTRO TIPO DE HECHO O AGRESIÓN Y ESPECIFIQUE, EN CASO DE QUE SUS HECHOS NO ESTÉN',
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ]
        },
        {
          type: 'text',
          title: 'NOMBRE COMPLETO DE LA VICTIMA O AFECTADO(A)',
          edit: false,
          f_id: 'q3',
          enable: true,
          number: '3',
          uppercaseAnswer: true,
          enableHelpMessage: true,
          helpMessage:
            'Tu nombre es ANÓNIMO, no se publicara ni se dará a conocer',
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ]
        },
        {
          type: 'title',
          edit: false,
          f_id: 'd12',
          enable: true,
          title: 'LUGAR DEL HECHO O SUCESO'
        },
        {
          type: 'options',
          title: 'ESTADO',
          edit: false,
          f_id: 'q13',
          enable: true,
          catalog: '5de6e7cf26811c001b8f2812-1575417143274',
          other: false,
          number: '4',
          required: true,
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ]
        },
        {
          type: 'options_dependant',
          title: 'MUNICIPIO',
          edit: false,
          f_id: 'q14',
          enable: true,
          number: '5',
          fk: 'q13',
          catalog: '5de6e7cf26811c001b8f2812-1575417194395',
          required: true,
          column: true,
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ]
        },
        {
          type: 'title',
          edit: false,
          f_id: 'd37',
          enable: true,
          title: 'DATOS DEL AGRESOR'
        },
        {
          type: 'text',
          title: 'PRIMER APELLIDO',
          edit: false,
          f_id: 'q19',
          enable: true,
          number: '6.1',
          uppercaseAnswer: true,
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ]
        },
        {
          type: 'text',
          title: 'SEGUNDO APELLIDO',
          edit: false,
          f_id: 'q20',
          enable: true,
          number: '6.2',
          uppercaseAnswer: true,
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ]
        },
        {
          type: 'text',
          title: 'NOMBRE(S)',
          edit: false,
          f_id: 'q21',
          enable: true,
          number: '6.3',
          uppercaseAnswer: true,
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ]
        },
        {
          type: 'number',
          title: 'EDAD',
          edit: false,
          f_id: 'q22',
          enable: true,
          number: '6.4',
          minimum: 10,
          prefix: '',
          suffix: false,
          maximum: 100,
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ]
        },
        {
          type: 'options',
          title: 'GENERO',
          edit: false,
          f_id: 'q23',
          enable: true,
          catalog: '5de6e7cf26811c001b8f2812-1575486262108',
          other: false,
          number: '6.5',
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ]
        },
        {
          type: 'photo',
          title: 'FOTOGRAFÍA DEL AGRESOR',
          edit: false,
          f_id: 'q29',
          enable: true,
          defaultCamera: 'FRONT',
          inputSource: 'BOTH',
          number: '6.6',
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ],
          manualTrigger: false,
          guide: false,
          faceDetection: false
        },
        {
          type: 'text',
          title: 'DIRECCIÓN DE VIVIENDA',
          edit: false,
          f_id: 'q24',
          enable: true,
          number: '6.7',
          uppercaseAnswer: true,
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ]
        },
        {
          type: 'text',
          title: 'COMENTARIO BREVE SOBRE EL AGRESOR',
          edit: false,
          f_id: 'q28',
          enable: true,
          number: '6.8',
          formatAnswer: 'multiple_line',
          enableHelpMessage: true,
          helpMessage:
            'ALGÚN TIPO DE DATO QUE AYUDE A SU SEGUIMIENTO Y CAPTURA',
          required: true,
          uppercaseAnswer: true,
          modes: [
            {
              enable: true,
              disabled: true,
              conditions: [{ type: 'status', status: 's1' }]
            }
          ]
        },
        {
          type: 'subtitle',
          edit: false,
          f_id: 'd26',
          enable: true,
          subtitle: 'EN BREVE ATENDEREMOS TU CASO'
        }
      ],
      elements: null,
      f_id_index: 37,
      parent_form_id: null,
      has_filled_forms: true,
      created_at: '2019-12-03T22:55:11.362Z',
      version: 37,
      print_template: false,
      pause: false,
      blocks: {},
      kiosk: null,
      ocr_webapi_key:
        'NTQwOTJjNjNkZGUwNDYyNzk3ODhiMjNjNDE4MDZiOTU6MDIzZGJkOTEtMjExZi00YTFmLWIyMjAtMDg4ZWFhNGJhNmM0',
      redirect: {},
      type: 'form'
    }
  ];

  function goNext() {
    setCurrentStep(currentStep + 1);
  }

  function goBack() {
    setCurrentStep(currentStep - 1);
  }

  return (
    <Wrapper>
      <Stepper
        steps={[
          { title: 'Step One' },
          { title: 'Step Two' },
          { title: 'Step Three' },
          { title: 'Step Four' }
        ]}
        activeStep={currentStep}
      />
      {data[0].questions.map((item, index) => {
        return <Text>{item.f_id}</Text>;
      })}
    </Wrapper>
  );
}

export default Form;
