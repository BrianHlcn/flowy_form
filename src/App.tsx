import React from 'react';
import './App.css';
import { Form } from './components';

const App: React.FC = () => {
  return <Form />;
};

export default App;
